<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public const USER_REFERENCE = 'user';

    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('admin@localhost');
        $admin->setPassword($this->passwordHasher->hashPassword(
            $admin,
            'test'
        ));
        $admin->addRole($this->getReference(RoleFixtures::ADMIN_ROLE_REFERENCE));
        $manager->persist($admin);

        $user = new User();
        $user->setEmail('user@localhost');
        $user->setPassword($this->passwordHasher->hashPassword(
            $user,
            'test'
        ));
        $user->addRole($this->getReference(RoleFixtures::USER_ROLE_REFERENCE));
        $manager->persist($user);
        $this->setReference(self::USER_REFERENCE, $user);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RoleFixtures::class,
        ];
    }
}
