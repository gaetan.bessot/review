<?php

namespace App\DataFixtures;

use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

class ReviewFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        /*for($i = 0; $i < 5; $i++)
        {
            $review = new Review();
            $review->setAuthor($this->getReference(UserFixtures::USER_REFERENCE));
            $review->setProduct($this->getReference(ProductFixtures::PRODUCT_REFERENCE));
            $review->setComment('Lorem ipsum');
            $review->setNote(random_int(0,4) + (random_int(0, 1) ? 0.5 : 1));
            $manager->persist($review);
        }

        $manager->flush();*/
    }

    public function getDependencies() : array
    {
        return [
            UserFixtures::class,
            ProductFixtures::class,
        ];
    }
}
