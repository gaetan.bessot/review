<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public const PRODUCT_REFERENCE = 'product';

    public function load(ObjectManager $manager)
    {
        /*$product = new Product();
        $product->setName('foobar');
        $product->setCategory($this->getReference(CategoryFixtures::CATEGORY_REFERENCE));
        $manager->persist($product);
        $this->addReference(self::PRODUCT_REFERENCE, $product);

        $manager->flush();*/
    }

    public function getDependencies(): array
    {
        return [
            CategoryFixtures::class,
        ];
    }
}
