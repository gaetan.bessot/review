<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*for($i = 0; $i < 5; $i++)
        {
            $picture = new Picture();
            $picture->setUrl('http://lorempixel.com/200/200/');
            $picture->setProduct($this->getReference(ProductFixtures::PRODUCT_REFERENCE));
            $manager->persist($picture);
        }

        $manager->flush();*/
    }

    public function getDependencies(): array
    {
        return [
            ProductFixtures::class,
        ];
    }
}
