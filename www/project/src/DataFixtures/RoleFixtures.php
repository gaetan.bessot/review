<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public const ADMIN_ROLE_REFERENCE = 'admin';
    public const USER_ROLE_REFERENCE = 'user';

    public function load(ObjectManager $manager)
    {
        $admin = new Role();
        $admin->setName('Administrateur');
        $admin->setCode('ROLE_ADMIN');
        $manager->persist($admin);
        $this->addReference(self::ADMIN_ROLE_REFERENCE, $admin);

        $user = new Role();
        $user->setName('Utilisateur');
        $user->setCode('ROLE_USER');
        $manager->persist($user);
        $this->addReference(self::USER_ROLE_REFERENCE, $user);

        $manager->flush();
    }
}
