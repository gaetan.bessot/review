<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Product;
use App\Form\PictureType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class PictureController extends AbstractController
{
    /**
     * @Route("/photo/supprimer/{picture}", name="picture_delete")
     */
    public function deletePicture(Picture $picture): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($picture);
        $manager->flush();

        return $this->redirectToRoute('product_info', [
            'product' => $picture->getProduct()->getId()
        ]);
    }

    /**
     * @Route("/photo/ajouter/{product}", name="picture_add")
     */
    public function addPicture(Request $request, Product $product, FileUploader $fileUploader): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $picture = new Picture();
        $form = $this->createForm(PictureType::class, $picture);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            // File upload service
            /** @var UploadedFile $pic */
            $pic = $form->get('url')->getData();
            $filename = $fileUploader->upload($pic);

            // Picture construction
            $picture->setUrl($filename)
                    ->setProduct($product)
                    ->setIsThumbnail($product->getPictures()->isEmpty());

            // Insert in database
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($picture);
            $manager->flush();

            return $this->redirectToRoute('product_info',[
                'product' => $product->getId()
            ]);
        }

        return $this->render('picture/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/photo/choisir-vignette/{picture}", name="picture_thumbnail")
     */
    public function setThumbnail(Picture $picture): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $picture->setIsThumbnail(true);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($picture);
        $manager->flush();

        return $this->redirectToRoute('product_info', [
            'product' => $picture->getProduct()->getId()
        ]);
    }
}
