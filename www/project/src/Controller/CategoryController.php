<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/categorie/ajouter", name="category_add")
     * @Route("/categorie/editer/{category}", name="category_edit")
     */
    public function addOrEditCategory(Request $request, Category $category = null): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // Get request type
        $isAddRequest = $category == null;

        // Form init
        if(!$category) $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        // Form processing
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            // Get form data
            $category = $form->getData();

            // Insert or update
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($category);
            $manager->flush();

            // Redirect to products list
            return $this->redirectToRoute('product_list_in_category', [
                'category' => $category->getId()
            ]);
        }

        // Render form page
        return $this->render('category/add.html.twig', [
            'form' => $form->createView(),
            'isAddRequest' => $isAddRequest
        ]);
    }
}
