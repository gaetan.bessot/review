<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Review;
use App\Form\ReviewType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends AbstractController
{
    /**
     * @Route("/avis/{product}", name="review_list_in_product")
     */
    public function reviewListInProduct(Product $product): Response
    {
        $reviews = $this->getDoctrine()
            ->getRepository(Review::class)
            ->findBy(
                ['product' => $product->getId()]
            );

        return $this->render('review/list.html.twig', [
            'product' => $product,
            'reviews' => $reviews
        ]);
    }

    /**
     * @Route("/avis/ajouter/{product}", name="review_add")
     */
    public function addReview(Request $request, Product $product): Response
    {
        $this->denyAccessUnlessGranted('add', new Review());

        $isAddRequest = true;

        $review = new Review();
        $review->setProduct($product);
        $review->setAuthor($this->getUser());
        $form = $this->createForm(ReviewType::class, $review);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $review = $form->getData();

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($review);
            $manager->flush();

            return $this->redirectToRoute('review_list_in_product', [
                'product' => $product->getId()
            ]);
        }

        return $this->render('review/add.html.twig', [
            'isAddRequest' => $isAddRequest,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/avis/editer/{review}", name="review_edit")
     */
    public function edit(Request $request, Review $review): Response
    {
        $this->denyAccessUnlessGranted('edit', $review);

        $isAddRequest = false;

        $form = $this->createForm(ReviewType::class, $review);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $review = $form->getData();

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($review);
            $manager->flush();

            return $this->redirectToRoute('review_list_in_product', [
                'product' => $review->getProduct()->getId()
            ]);
        }

        return $this->render('review/add.html.twig', [
            'isAddRequest' => $isAddRequest,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/avis/supprimer/{review}", name="review_delete")
     */
    public function delete(Review $review): Response
    {
        $this->denyAccessUnlessGranted('delete', $review);

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($review);
        $manager->flush();

        return $this->redirectToRoute('review_list_in_product', [
            'product' => $review->getProduct()->getId()
        ]);
    }
}
