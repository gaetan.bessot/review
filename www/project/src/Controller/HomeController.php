<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Form\CategorySelectorType;
use App\Form\ChoiceCategoryType;
use Doctrine\ORM\EntityRepository;
use Exception;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Choice;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request): Response
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        $form = $this->createForm(CategorySelectorType::class);
        $form->handleRequest($request);

        return $this->render('home/index.html.twig', [
            'categories' => $categories,
            'products' => $form->isSubmitted() && $form->isValid() ? ProductController::getChildrenProducts($form->get('category')->getData()) : $products,
            'form' => $form->createView()
        ]);
    }
}
