<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Picture;
use App\Entity\Product;
use App\Form\ProductType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/produits/ajouter", name="product_add")
     * @Route("/produits/editer/{product}", name="product_edit")
     */
    public function addProduct(Request $request, Product $product = null): Response
    {
        // Check permission
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // Get request type
        $isAddRequest = $product == null;

        // Create form
        if(!$product) $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        // Process form
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            // Get form data
            $product = $form->getData();

            // Add in database
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($product);
            $manager->flush();

            // Redirect on product page
            return $this->redirectToRoute('product_info', [
                'product' => $product->getId()
            ]);
        }

        return $this->render('product/add.html.twig', [
            'form' => $form->createView(),
            'isAddRequest' => $isAddRequest
        ]);
    }

    /**
     * @Route("/produits/{category}", name="product_list_in_category")
     */
    public function productsListInCategory(Category $category): Response
    {
        return $this->render('product/list.html.twig', [
            'category' => $category,
            'products' => $this->getChildrenProducts($category)
        ]);
    }

    /**
     * @Route("/produits/info/{product}", name="product_info")
     */
    public function productInfo(Product $product): Response
    {
        return $this->render('product/info.html.twig', [
            'product' => $product
        ]);
    }

    public static function getChildrenProducts(Category $category): Collection
    {
        // Get parent category products
        $products = $category->getProducts();

        // Get children categories products
        foreach($category->getChildrens() as $child) {
            if(gettype($products) != "array")
                $products = $products->toArray();
            $products = array_merge($products, ProductController::getChildrenProducts($child)->toArray());
        }

        return new ArrayCollection(gettype($products) == "array" ? $products : $products->toArray());
    }
}
