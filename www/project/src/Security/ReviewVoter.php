<?php

namespace App\Security;

use App\Entity\Review;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ReviewVoter extends Voter
{
    const DELETE = 'delete';
    const EDIT = 'edit';
    const ADD = 'add';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if(!in_array($attribute, [self::EDIT, self::DELETE, self::ADD])) {
            return false;
        }

        if(!($subject instanceof Review)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if(!($user instanceof User)) {
            return false;
        }

        $review = $subject;

        switch($attribute)
        {
            case self::ADD:
                return $this->security->isGranted('ROLE_USER');
            case self::DELETE:
            case self::EDIT:
                return $user === $review->getAuthor() or $this->security->isGranted('ROLE_ADMIN');
        }

        return false;
    }
}